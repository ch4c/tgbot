package main;


import com.google.inject.internal.cglib.core.$ClassEmitter;
import org.telegram.telegrambots.ApiContextInitializer;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboard;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

import java.util.ArrayList;
import java.util.List;


public class Bot extends TelegramLongPollingBot {
    boolean config = false;
    boolean names = false;

    public void sendMsg(Message message, String text) {
        SendMessage sendMessage = new SendMessage();
        // sendMessage.enableMarkdown(true);
        sendMessage.setChatId(message.getChatId().toString());
      //  sendMessage.setReplyToMessageId(message.getMessageId());
        sendMessage.setText(text);
        try {
            setButtons(sendMessage);
            execute(sendMessage);
        }   catch (TelegramApiException e) {
            e.printStackTrace();
        }

    }

    public void onUpdateReceived(Update update) {
        Message message = update.getMessage();
        String text = message.getText();
        if ("/help".equals(text)) {
            sendMsg(message, "Списки формируются случайным образом. \n Если количество людей некратно количеству групп, то сформированные группы будут разными по размеру, но отличие не > 1. \n \n Вернуться назад — /config ");
        }
        if ("/start".equals(text)) {
            sendMsg(message, "Привет!" + "\n" + "\n" + "Используте /config чтобы задать параметры для рандомайзера");
        }
        if (config == true) {
            config = false;
            String namesString = text;
            Randomizer random = new Randomizer();
            String output = random.Rnd(namesString);
            sendMsg(message, "Сформированные группы:" + "\n" + "\n" + output); // ну и тут вывод, пока не знаю как будет реализован
        }
        if ("/config".equals(text)) {
            sendMsg(message, "Введите цифру — количество групп, затем имена через пробел:" +  "\n" + "\n" + "Наример," +  "\n" + "2 Артур Дамир Камиль" + "\n" + "\n" + "Непонятно? — /help"  );
            config = true;

        }

    }




    public void setButtons(SendMessage sendMessage) {
        ReplyKeyboardMarkup replyKeyboardMarkup = new ReplyKeyboardMarkup();
        sendMessage.setReplyMarkup(replyKeyboardMarkup);
        replyKeyboardMarkup.setSelective(true);
        replyKeyboardMarkup.setResizeKeyboard(true);
        replyKeyboardMarkup.setOneTimeKeyboard(false);

        List <KeyboardRow> keyboardRowList = new ArrayList<>();
        KeyboardRow keyboardFirstRow = new KeyboardRow();

        keyboardFirstRow.add(new KeyboardButton("/start"));
       // keyboardFirstRow.add(new KeyboardButton( "test too"));

        keyboardRowList.add(keyboardFirstRow);
        replyKeyboardMarkup.setKeyboard(keyboardRowList);


    }



    public String getBotUsername() {
        return "RndmzrBot";
    }

    public String getBotToken() {
        return "1025929807:AAHHaJ6DU3NnsBwEonj6KBKV7vJp06IOtDo";
    }
}
